# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## **How to install Angular**
First of all, you need to install node.js on your system.
1. Run the following commands to install the latest version on node.js (Ubuntu):\
   ```sudo apt install python-software-properties```\
```curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -```\
```sudo apt install nodejs```

2. Make sure you have successfully installed node.js and NPM on your system:\
   ```node --version```\
   ```npm --version```
3. After installation of node.js and npm on your system, use following commands to install Angular cli tool on your system.\
```npm install -g @angular/cli```

1. The latest version of Angular CLI will be installed on your Ubuntu Linux system. You may require older Angular version on your machine. To install specific Angular version run command as following with version number.Example for version 7:\
   ```npm install -g @angular/cli@7```
2. Check the installed version of ng on your system:\
   `ng --version`

3. Make sure you're in the Frontend directory, then run:\
   `npm install` to install all needed packages.
   
## Known Errors

### **Error: ENOSPC: System limit for number of file watchers reached**
`Ubuntu`
1. Modify number of system monitoring files.
```
sudo nano /etc/sysctl.conf
```
2. Add a line at the buttom.
```
fs.inotify.max_user_watches=524288
```
3. Then save and exit(CTRL+S -> CTRL+X)! To check it: 
```
sudo sysctl -p
```




