import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CarService } from '../shared/services/car.service';

@Component({
  selector: 'app-service-calendar',
  templateUrl: './service-calendar.component.html',
  styleUrls: ['./service-calendar.component.css']
})

export class ServiceCalendarComponent implements OnInit {

  constructor(private service: CarService, private toastr: ToastrService, private router : Router) { }
  serviceCalendars: any;
  ngOnInit(): void {
    if(localStorage.getItem("token") != null){
      const exp = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]))['exp'];
      // navigate to login page when token expired
      if (Date.now() >= exp * 1000) {
        this.router.navigateByUrl('/user/login');
        return;
      }      
    }
   
      this.service.getCars().subscribe(
        res => { 
         this.serviceCalendars = this.mapData(res);
         if(res['token'] != ""){
           // updating token in local storage only when expired
          localStorage.setItem('token', res['token']);
          console.log("updated token: get-cars");
          }
        },
        err => {
          if(err.status != 401){
            this.toastr.error("Error on getting car calendars.");
          }
          console.log(err);
        })
  }

 
  mapData(res)
  {
    var serviceCalendars = [];
    const oneDay = 24 * 60 * 60 * 1000;
    const currentDate = new Date();
    res.cars.forEach(item => {
      var carCalendar;
      var carNameAndYear = item.Name + " " + item.Year;
      var actualKilometers = item.ActualKilometers;
      var nextRevisionKm = this.mapKm(item.LastRevisionKm + 10000);

      var nextRevisionDateAux = new Date(item.LastRevisionDate);
      nextRevisionDateAux.setFullYear(nextRevisionDateAux.getFullYear() + 1);
      var nextRevisionDate = this.mapDate(nextRevisionDateAux.toString())
       
      var nextPtiAux = new Date(item.LastPti);
      nextPtiAux.setFullYear(nextPtiAux.getFullYear() + 1);
      var nextPti = this.mapDate(nextPtiAux.toString())

      var nextVigAux = new Date(item.LastVig);
      nextVigAux.setFullYear(nextVigAux.getFullYear() + 1);
      var nextVig = this.mapDate(nextVigAux.toString());

      var nextInsuranceAux = new Date(item.LastInsurance);
      nextInsuranceAux.setFullYear(nextInsuranceAux.getFullYear() + 1);
      var nextInsurance = this.mapDate(nextInsuranceAux.toString());

      var remainingCalendar = {}
      var remainingRevisionKm = this.mapKm(nextRevisionKm - actualKilometers)
      
      var remainingRevisionDays = this.mapDays(Math.floor((nextRevisionDateAux.getTime() - currentDate.getTime()) / oneDay));
      var remainingPtiDays = this.mapDays(Math.floor((nextPtiAux.getTime() - currentDate.getTime()) / oneDay));
      var remainingVigDays = this.mapDays(Math.floor((nextVigAux.getTime() - currentDate.getTime()) / oneDay));
      var remainingInsuranceDays = this.mapDays(Math.floor((nextInsuranceAux.getTime() - currentDate.getTime()) / oneDay));
  
      remainingCalendar = {
        remainingRevisionKm : remainingRevisionKm,
        remainingRevisionDays : remainingRevisionDays,
        remainingPtiDays : remainingPtiDays,
        remainingVigDays : remainingVigDays,
        remainingInsuranceDays : remainingInsuranceDays
      }
      carCalendar = {
        carNameAndYear : carNameAndYear,
        actualKilometers : actualKilometers,
        nextRevisionKm : nextRevisionKm,
        nextRevisionDate : nextRevisionDate,
        nextPti : nextPti,
        nextVig: nextVig,
        nextInsurance : nextInsurance,
        remainingCalendar : remainingCalendar
      }
      serviceCalendars.push(carCalendar)
    })
  return serviceCalendars;
  }

  mapKm(stringValue){
    if(stringValue <= 500 && stringValue >=0){
      return stringValue + " (Soon)";
    }
    if(stringValue < 0 ){
      return stringValue + " (Expired)"
    }
    return stringValue;
  }

  mapDate(stringValue){
    return moment(stringValue).format("DD-MM-YYYY");
  }

  mapDays(stringValue){
    if(stringValue <= 30 && stringValue > 0){
      return stringValue + " (Soon)";
    }
    if(stringValue == 0){
      return stringValue + " (Today)";
    }
    if(stringValue < 0){
      return stringValue + " (Expired)"
    }
    return stringValue;
  
  } 

  getStyleByTextValue(textValue){
    textValue = textValue.toString();
    if(textValue.includes('(Soon)'))
    {
      return{
        'color': '#ffcc00',
        'font-weight':  'bold'
      }
    }
    else if(textValue.includes('(Expired)') || textValue.includes('(Today)'))
    {
      return {
        'color' : '#dd0000',
        'font-weight':  'bold'
      }
    }
      return;
  }
 
}


