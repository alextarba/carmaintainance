import { Component, OnInit, OnDestroy } from '@angular/core';
import * as $ from "jquery" ;
import { ToastrService } from 'ngx-toastr';
import { Router, NavigationEnd } from '@angular/router';
import { CarService } from 'src/app/shared/services/car.service';

@Component({
  selector: 'app-list-cars',
  templateUrl: './list-cars.component.html',
  styleUrls: ["./list-cars.component.css"]
})
export class ListCarsComponent implements OnInit, OnDestroy{
  numericalPattern = "^\\d+$";
  currentDate = new Date();
  cars;
  mySubscription: any;
  constructor(private service:CarService, private toastr: ToastrService, private router: Router) {
  
this.router.routeReuseStrategy.shouldReuseRoute = function () {
  return false;
};
this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    if(localStorage.getItem("token") != null){
      const exp = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]))['exp'];
      // navigate to login page when token expired
      if (Date.now() >= exp * 1000) {
        this.router.navigateByUrl('/user/login')
      }      
    }
    this.currentDate.setDate(new Date().getDate()  + 1)
    this.initCars();
  }

  initCars(){
    this.service.getCars().subscribe(
      res => {
         this.cars = res['cars'];
         if(res['token'] != ""){
           // updating token in local storage only when expired
          localStorage.setItem('token', res['token']);
          console.log("updated: get-cars")
         }
         this.cars.forEach((item)=>{
              item.LastRevision = new Date(item.LastRevision);
              item.LastPti= new Date(item.LastPti);
              item.LastVig = new Date(item.LastVig);
              item.LastInsurance = new Date(item.LastInsurance);
          })
          
      },
      err =>{
        if(err.status != 401){
          this.toastr.error("Error on getting cars")
        }
      }
    )
  }
  onRowRemoving(e) {
    let d = $.Deferred();  
    this.service.removeCar(e.key.Name).subscribe(
      res => {
        this.toastr.success("Car " + e.key.Name + " successfully deleted.");
        if(res['token'] != ""){
          // updating token in local storage only when expired
         localStorage.setItem('token', res['token']);
         console.log("token updated remove-car");
        }
        d.resolve();  
      },
      err =>{
        if(err.status != 401){
          this.toastr.error("Error on removing a car")
        }
        console.log(err);
        d.reject();
      }
    );
    e.cancel = d.promise();
  }
  onRowUpdating($event){
    let d = $.Deferred();  
    this.service.updateCar({...$event.oldData, ...$event.newData}).subscribe(
      res => {
        this.toastr.success("Car updated successfully");
        if(res['token'] != ""){
          // updating token in local storage only when expired
         localStorage.setItem('token', res['token']);
         console.log("updated token: update-car")
        }
        d.resolve();  
      },
      err =>{
        if(err.status != 401){
          this.toastr.error("Error on updating a car")
        }
        console.log(err);
        d.reject();
      }
    );
    $event.cancel = d.promise();
  }
  
ngOnDestroy() {
  if (this.mySubscription) {
    this.mySubscription.unsubscribe();
  }
}
}
