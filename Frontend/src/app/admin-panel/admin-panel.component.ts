import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { DxDataGridComponent } from 'devextreme-angular';
import * as $ from "jquery" ;
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ["./admin-panel.component.css"]
})


export class AdminPanelComponent implements OnInit {
  constructor(private service: UserService, private toastr: ToastrService, private router: Router) { }
  customers;
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent
  ngOnInit(): void {
    
    if(localStorage.getItem("token") != null){
      const exp = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]))['exp'];
      // navigate to login page when token expired
      if (Date.now() >= exp * 1000) {
        this.router.navigateByUrl('/user/login')
      }      
    }

    this.service.getCustomers().subscribe(
      res => {
         this.customers = res['data'];
         this.dataGrid.instance.option("errorRowEnabled", false);
         if(res['token'] != ""){
         // updating token in local storage only when expired
         localStorage.setItem('token', res['token']);
         console.log("updated: get-customers");
         }
      },
      err =>{
        if(err.status != 401){
          this.toastr.error("Error on getting customers")
          console.log(err);
        }
      }
    )
  }

  onRowRemoving(e) {
    let d = $.Deferred();  
    this.service.removeCustomer(e.key.userName).subscribe(
      res => {
        this.toastr.success("Customer " + e.key.userName + " successfully deleted.")
        d.resolve(); 
        if(res['token'] != ""){
          // updating token in local storage only when expired
          localStorage.setItem('token', res['token']);
          console.log("updated: remove-customer")
        }
      },
      err =>{
        if(err.status != 401){
          this.toastr.error("Error on deleting customer")
        }
        console.log(err);
        d.reject();
      }
    );
    e.cancel = d.promise();
  }

}
