import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { HomeCarsComponent } from './cars/home-cars/home-cars.component';
import { ServiceCalendarComponent } from './service-calendar/service-calendar.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

const routes: Routes = [
  {
    path:'user', component:UserComponent, 
      children: [
        {path:'registration', component:RegistrationComponent},
        {path:'login', component:LoginComponent}

      ]
  },
  {
    path:'adminpanel', component:AdminPanelComponent, canActivate:[AuthGuard], data: {permittedRoles:['admin']}
  },
  {
    path:'home', component:HomeComponent, canActivate:[AuthGuard], data: {permittedRoles:['admin','user']}
  },
  {
    path:'mycars', component: HomeCarsComponent, canActivate:[AuthGuard], data: {permittedRoles:['user']}
  },
  {
    path:'forbidden', component:ForbiddenComponent
  },
  {
    path:'serviceCalendar', component: ServiceCalendarComponent, canActivate:[AuthGuard], data: {permittedRoles:['user']}
  },
  {
    path:'**', redirectTo:'/user/login', pathMatch:'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
