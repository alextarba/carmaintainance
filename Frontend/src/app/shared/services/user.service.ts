import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private fb:FormBuilder, private http:HttpClient) { }
  readonly  BaseURI = "http://localhost:5000/api/v1";
  formModel = this.fb.group({
    UserName : ['', Validators.required],
    Email : ['', Validators.email],
    FullName : [''],
    Passwords : this.fb.group({
      Password : ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword : ['',  Validators.required]
    }, {validator : this.comparePasswords})
  })

  register(){
    var body = {
      UserName: this.formModel.value.UserName,
      Email: this.formModel.value.Email,
      FullName: this.formModel.value.FullName,
      Password: btoa(this.formModel.value.Passwords.Password)
    }
    return this.http.post(this.BaseURI + "/user/register", body);
  }

  login(formData){
    // converting password to base64
    formData.Password = btoa(formData.Password);
    return this.http.post(this.BaseURI + "/user/login", formData);
  }

  removeCustomer(userName:string){
    return this.http.post(this.BaseURI+'/admin/remove_user', {token: localStorage.getItem('token'), userName})
  }

  getCustomers(){
    return this.http.post(this.BaseURI+'/admin/get_users', {token: localStorage.getItem('token')})
  }

  roleMatch(allowedRoles): boolean {
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if (userRole == element) {
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }

  comparePasswords(fb:FormGroup){
    let confirmPasswordControl = fb.get('ConfirmPassword');
    
    if(confirmPasswordControl.errors == null || 'passwordMismatch' in confirmPasswordControl.errors ){
      if(fb.get('Password').value != confirmPasswordControl.value ){
        confirmPasswordControl.setErrors({passwordMismatch : true})
      }
      else
      {
        confirmPasswordControl.setErrors(null);
      }
    }
  }
}
