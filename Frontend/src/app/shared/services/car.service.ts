import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http:HttpClient) { }
  readonly  BaseURI = "http://localhost:5000/api/v1";

  insertCar(carDetails:Object){
    // var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    // // adding username to body
    // carDetails['username'] = payLoad.sub;
    return this.http.post(this.BaseURI+'/car/insert', {token: localStorage.getItem('token'), carDetails: carDetails});
  }

  getCars(){
    // var payLoad = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    return this.http.post(this.BaseURI+'/car/get_user_cars', {token: localStorage.getItem('token')});
  }

  removeCar(carId){
    return this.http.post(this.BaseURI+'/car/remove', {token: localStorage.getItem('token'), carId: carId});
  }

  updateCar(resource){
      var body = {
        Name: resource.Name,
        ActualKilometers: resource.ActualKilometers,
        LastRevisionKm: resource.LastRevisionKm,
        LastRevisionDate: resource.LastRevisionDate,
        LastPti: resource.LastPti,
        LastVig: resource.LastVig,
        LastInsurance: resource.LastInsurance
      }
      return this.http.post(this.BaseURI + "/car/update_car", {token: localStorage.getItem('token'), carDetails: body});
  }
}
