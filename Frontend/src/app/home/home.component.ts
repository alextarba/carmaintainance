import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    if(localStorage.getItem("token") != null){
      const exp = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]))['exp'];
      // navigate to login page when token expired
      if (Date.now() >= exp * 1000) {
        this.router.navigateByUrl('/user/login');
        this.toastr.error("Authentication time expired!");
      }      
    }
  }
  
}
