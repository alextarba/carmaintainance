## Install and run frontend application

Is preferred to use Visual Studio Code to edit / start this project.

Open 'Frontend' folder in VSCode then start a terminal and run ```ng serve -o```.