#!/bin/bash

cd Backend
# Start backend (flask) server
chmod a+x ./start.sh
gnome-terminal -- sh start.sh

# Move to the Frontend directory now
cd ../Frontend/
# Start frontend (angular) server
ng serve
