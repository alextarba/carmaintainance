import jwt
import datetime
from util.util import get_user_role_from_expired_token

def encode_auth_token(user_name: str, secret_key: str, role: str):
    """
    Generates the Auth Token
    :param user_name: string 
    :param secret_key: string
    :return: string
    """
    try:
        payload = {
            # expiration date of the token
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=30),
            # the time the token is generated
            'iat': datetime.datetime.utcnow(),
            # the subject of the token (the user whom it identifies)
            'sub': user_name,
            'role' : role
        }
        return jwt.encode(
            payload,
            secret_key,
            algorithm='HS256'
        )
    except Exception as e:
        return e

def decode_auth_token(token, secret_key):
    """
    Decodes the auth token
    :param token:
    :param secret_key: string
    :return: according username and role inside application
    """
    try:
        payload = jwt.decode(token, secret_key, algorithms=["HS256"])
        return payload
    except jwt.ExpiredSignatureError:
        # On signature expired raise same error to server
        raise jwt.ExpiredSignatureError("Token signature expired!")
    except jwt.InvalidTokenError:
        return "Invalid token!"

def get_user_and_role_from_token(current_token, secret_key):
    """
    Decodes current token received from frontend and returns user and and role.
    """
    # current token
    user = ""
    role = ""
    # current payload (it may be another token if this one expired)
    try:
        payload = decode_auth_token(current_token, secret_key)
        if 'sub' in payload:
            #token still active and it could retrieve user and role
            return payload['sub'], payload['role']
    except jwt.ExpiredSignatureError as e:
        # On signature expired raise same error to server
        raise e

    # returning nothing otherwise
    return None

def get_user_and_token_from_token(current_token, secret_key):
    """
    Refresh the token when it's active and throw an Error otherwise.
    """
    try:
        user, role = get_user_and_role_from_token(current_token, secret_key)
        token = encode_auth_token(user, secret_key, role)
        return user, token
    except jwt.ExpiredSignatureError as e:
        # On signature expired raise same error to server
        raise e
    # returning nothing otherwise
    return None