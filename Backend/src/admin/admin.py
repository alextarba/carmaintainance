from person.person import Person

class Admin(Person):
    def __init__(self, email, fullname, password):
        """
        Initializes the instance.
        """
        super().__init__(email, fullname, password)
        pass
