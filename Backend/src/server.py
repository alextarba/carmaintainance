from flask import Flask
from database.mongo.mongo_person import MongoDBPerson
from database.mongo.mongo_car import MongoDBCars
from util.util import get_user_role_from_expired_token, get_user_car_list
from flask import request,jsonify
from flask import Response
from flask_cors import CORS
import base64
import json
import datetime
import jwt
from user.user import User
from car.car import *

from security.security import *

app = Flask(__name__)

# Avoiding CORS errors by enabling all CORS origins
cors = CORS(app, resources={r"/api/v1*": {"origins": "*"}})

# instantiate connection to mongo db users collection
users_db = MongoDBPerson()
cars_db = MongoDBCars()
secret_key = 'super-secret'

@app.route('/')
def hello_world():
    return 'Hello, CarMaintainance!'

@app.route("/api/v1/user/register", methods=["POST"])
def register():
    # Predefine response on succes 
    success = {
        "succeeded": True,
        "errors": []
    }

    # Predefined response on user already exists
    failure = {
            "succeeded": False,
            "errors": [{
                "code": "DuplicateUserName"
            }]
           }

    data = request.get_json()    
    user = User(data['UserName'], data['Password'], 'user', data['Email'], data['FullName'])
    user_exists = users_db.read({"username": user.username})

    # checking to see if user already exists in database
    if user_exists.count() == 0:
        # user not found
        users_db.create(user)
        return success
    else:
        return failure

@app.route("/api/v1/user/login", methods=["POST"])
def login():
    data = request.get_json()
    user = User(data['UserName'], data['Password'])
    print(user.to_short_dict())
    user_exists = users_db.read(user.to_short_dict())
    # checking to see if user already exists in database
    if user_exists.count() == 0:
        # user not found
        return {}, 400
    else:
        # a match was found
        role = user_exists[0]['role']
        token = encode_auth_token(user.username, secret_key, role)
        return { 
            "token": token,
            "role": role
            }, 200


@app.route("/api/v1/car/insert", methods=["POST"])
def insert_car(): 
    data = request.get_json()
    # getting username and token from token
    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401

    data = data['carDetails']
    car = Car(data[NAME], data[DETAILS], int(data[YEAR]), int(data[KILOMETERS]), int(data[LAST_REVISION_KM]),
            convert_from_json_to_datetime(data[LAST_REVISION_DATE]),
            convert_from_json_to_datetime(data[LAST_PTI]),
            convert_from_json_to_datetime(data[LAST_VIG]),
            convert_from_json_to_datetime(data[LAST_INSURANCE])
            )
    car_exists = cars_db.read({'username': user, 'name': data[NAME]})

    # checking to see if car already exists in database
    if car_exists.count() == 0:
        # car not found
        cars_db.create(car, user)
        return {'token': token}, 200
    else:
        return {'token': token}, 409 


@app.route("/api/v1/car/remove", methods=["POST"])
def remove_car():
    data = request.get_json()
    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401

    # removing correspondent car based on name
    cars = cars_db.delete({'username': user, NAME: data['carId']})
    return {'token': token}

@app.route("/api/v1/car/get_user_cars", methods=["POST"])
def get_user_cars():
    data = request.get_json()
    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401

    cars = cars_db.read({'username': user})

    # checking to see if there are cars in database
    if cars.count() == 0:
        # cars not found
        return {
            'token': token
        }
    else:
        list_of_cars = []
        for result in cars:
            current = Car(
                        result[NAME],
                        result[DETAILS],
                        result[YEAR],
                        result[KILOMETERS],
                        result[LAST_REVISION_KM],
                        result[LAST_REVISION_DATE],
                        result[LAST_PTI],
                        result[LAST_VIG],
                        result[LAST_INSURANCE])
            list_of_cars.append(current.to_json())
        return {
            'token': token,
            'cars': list_of_cars
        }

@app.route("/api/v1/car/update_car", methods=["POST"])
def update_car():
    data = request.get_json()
    print(data)

    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401

    car_details = data['carDetails']
    cars_db.update(criteria={
            'username': user,
            NAME: car_details[NAME],
            }, update_value={
            KILOMETERS: car_details[KILOMETERS],
            LAST_REVISION_KM: car_details[LAST_REVISION_KM],
            LAST_REVISION_DATE: car_details[LAST_REVISION_DATE],
            LAST_PTI: car_details[LAST_PTI],
            LAST_VIG: car_details[LAST_VIG],
            LAST_INSURANCE: car_details[LAST_INSURANCE]
            })

    return {'token': token},200

@app.route("/api/v1/admin/get_users", methods=["POST"])
def get_users():
    data = request.get_json()

    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401

    user_exists = users_db.read({'role': 'user'})
    list_of_users = []
    for user in user_exists:
        car_list = get_user_car_list(user['username'], cars_db)
        list_of_users.append({
            'userName': user['username'],
            'fullName': user['fullname'],
            'email': user['email'],
            'carList': car_list
            })
    return {
            'token': token,
            'data': list_of_users
        }, 200

@app.route("/api/v1/admin/remove_user", methods=["POST"])
def remove_user():
    data = request.get_json()
    print(data)
    try:
        user, token = get_user_and_token_from_token(data['token'],secret_key)
    except jwt.ExpiredSignatureError as e:
        return {'message': str(e)}, 401
    
    users_db.delete({'username': data['userName']})
    return {'token': token}, 200

if __name__ == "__main__":
    app.run(debug=True)