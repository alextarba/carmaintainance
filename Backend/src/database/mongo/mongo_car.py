import sys

from pymongo import MongoClient
from database.abstract_db import AbstractDB
from car.car import Car

class MongoDBCars(AbstractDB):
    def __init__(self, host=None, port=None):
        if host and port:
            self._host = host
            self._port = port
        else:
            # Default url and ports used by mongo DB
            self._host = 'localhost'
            self._port = 27017
        self._url = self._host + ":" + str(self._port)

        # Test the connection to MongoDB
        self.test_connection(self._url) 

        # database carmaintenance, collection cars
        self.cars = self._client.carmaintenance.cars   

    def test_connection(self, url):
        try:
            self._client = MongoClient(host=url, serverSelectionTimeoutMS = 2000)
            self._client.server_info()
        except:
            print(f"Connection refused to '{self._host}:{self._port}'")
            sys.exit(1)

    def create(self, car, username):
        """ 
        Inserts a car inside cars collection from database.

        Arguments:
            car: an object from class Car to be inserted inside database.
            username: owner of the car.
        """

        if not car or not isinstance(car, Car):
            raise ValueError("Wrong argument passed", car)
        self.cars.insert_one(car.to_dict(username))

    def read(self, criteria: dict):
        """ 
        Reading one user car/cars depending on the criteria.

        Arguments:
            criteria: an object having a key as username and an optional name in order to search for specific car.
                      otherwise it will be returned all cars.
        Returns:
            An object if the criteria was found inside collection or None otherwise
        """
        return self.cars.find(criteria)        

    def update(self, criteria, update_value):
        """ 
        Updates a car with new informations.

        Arguments:
            criteria: the object which will be updated inside collection based on username and car Name
            update_value: the field or fields which will be updated
        """
        if not criteria:
            raise ValueError("Wrong argument passed", criteria)

        if not update_value:
            raise ValueError("Wrong argument passed", update_value)
        
        self.cars.update_one(criteria, {"$set": update_value})

    def delete(self, criteria):
        """ 
        Deletes a car from the collection

        Arguments:
            criteria: an object having a key as username and an optional name in order to search for specific car.
                      otherwise it will be returned all cars.
        """
        if not criteria:
            raise ValueError("Wrong argument passed", criteria)

        self.cars.delete_one(criteria)
