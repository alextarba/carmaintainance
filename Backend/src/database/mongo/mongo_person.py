import sys
import pymongo 
from pymongo import MongoClient
from database.abstract_db import AbstractDB
from user.user import User
from util.util import encode_to_base64

class MongoDBPerson(AbstractDB):
    def __init__(self, host=None, port=None):
        if host and port:
            self._host = host
            self._port = port
        else:
            # Default url and ports used by mongo DB
            self._host = 'localhost'
            self._port = 27017
        self._url = self._host + ":" + str(self._port)

        # Test the connection to MongoDB
        self.test_connection(self._url) 

        # database carmaintenance, collection users
        self.users = self._client.carmaintenance.users   

    def test_connection(self, url):
        try:
            self._client = MongoClient(host=url, serverSelectionTimeoutMS = 2000)
            self._client.server_info()
        except:
            print(f"Connection refused to '{self._host}:{self._port}'")
            sys.exit(1)

    def create(self, user):
        """ 
        Inserts an user inside user collection from database.

        Arguments:
            user: an object from class User to be inserted inside database.
        """

        if not user or not isinstance(user, User):
            raise ValueError("Wrong argument passed", user)
        self.users.insert_one(user.to_dict())

    def read(self, criteria: dict):
        """ 
        Usually criteria means that we only accept unique usernames and emails.
        So an user cannot create multiple accounts using the same email or same username for different emails.

        Arguments:
            criteria: an object having two keys username and email to find corresponding user from collection
        Returns:
            An object if the criteria was found inside collection or None otherwise
        """
        return self.users.find(criteria)        

    def update(self, old_user, new_user):
        """ 
        Updates an user with new informations.

        Arguments:
            old_user: the object which will be updated inside collection
            new_user: the object that will replace the old_user
        """
        if not old_user or not isinstance(old_user, User):
            raise ValueError("Wrong argument passed", old_user)

        if not new_user or not isinstance(new_user, User):
            raise ValueError("Wrong argument passed", new_user)

        self.users.update_one(old_user.to_dict(), {"$set": new_user.to_dict()})

    def delete(self, criteria: dict):
        """ 
        Deletes an user from the collection based on criteria

        Arguments:
            criteria: a criteria based on will remove users, type dict.
        """
        
        self.users.delete_one(criteria)


    def create_admin(self, username, password):
        """ 
        Inserts an admin inside user collection from database(only for developers).

        Arguments:
            username: the username of the admin
            password: password of the admin
        """

        if username == "" or password == "":
            raise ValueError("Wrong argument passed")
        self.users.insert_one({'username': username, 'password': encode_to_base64(password), 'role': 'admin'})
