from mongo.mongo_person import MongoDBPerson
from mongo.mongo_car import MongoDBCars
from user.user import User
from car.car import Car
from util.util import encode_to_base64
import datetime

mongo = MongoDBPerson()
# # Add user to DB
# user = User("testUsername","samplePassword", "test@email.com","Full Name",)
# mongo.create(user)

mongo.create_admin('lucian_admin', 'parola')
# # Read user from database based on criteria
result = mongo.read({
})
print(result.count())
for user in result:
    print(user)
print("User:",result)

# Update an existing user with another one

# new_user = User("updatedUsername","updatedPassword", "updated@email.com", "Updated Full Name")
# mongo.update(user,new_user)

# result = mongo.read({
#     "username":"updatedUsername",
# })
# print("Updated user:",result)

# # Delete all users from database
# mongo.delete({
    
# })

# # Delete one user from database
# mongo.delete({
#   'username': 'test'
# })


"""Examples for Car database."""
# cars_db = MongoDBCars()
# # car = Car("BMW", "No details", 1998, 21500, 20000, datetime.datetime(2002, 10, 27, 6, 0, 0),datetime.datetime(2002, 10, 27, 6, 0, 0),datetime.datetime(2002, 10, 27, 6, 0, 0),datetime.datetime(2002, 10, 27, 6, 0, 0))
# # cars_db.create(car,"test")

# result = cars_db.read({'username':'lucian_tarba'})
# print(result.count())
# for res in result:
#     print(res['Name']+ str(res['Year']))


# new_car = Car("Audi", "No details", 2012, 21500, 20000, datetime.datetime.now(),datetime.datetime.now(),datetime.datetime.now(),datetime.datetime.now())
# result = cars_db.read({})
# print(result.count())
# for car in result:
#     print(car['name'] + str(car['year']))

# cars_db.update({'username': 'lucian_tarba','Name':"Logan"},{'Year':2021})

# cars_db.delete({'username':'lucian_tarba','Name':'Logan'},)

