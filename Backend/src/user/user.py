from typing import Any
from person.person import Person

class User(Person):
    def __init__(self, username: str, password: str, role=None, email=None, fullname=None):
        """
        Initializes the instance.
        """
        super().__init__(password, email, fullname)
        self.username = username
        if role:
            self.role = role
    
    @property
    def username(self) -> str:
        """
        Retrieves the username.
        """
        return self._username
    
    @username.setter
    def username(self, username: str):
        """
        Sets the  username

        Args:
            username: The choosen username.
        """
        if username is None or username == "":
            raise ValueError("Must supply a valid name!")

        self._username = username

    @property
    def role(self) -> str:
        """
        Retrieves the user role.
        """
        return self._role
    
    @role.setter
    def role(self, role: str):
        """
        Sets the  role of current user. 

        Args:
            role: a string from: {user, admin}
        """
        if role is None or role == "" or role not in ['user', 'admin']:
            raise ValueError("Must supply a valid role (user or admin)!")

        self._role = role


    def to_dict(self):
        """
        Returns the dict version of the user object
        In order to be used in mongo db.
        """
        return {
            "username": self.username,
            "password": self.password,
            "email": self.email,
            "fullname": self.fullname,
            "role": self.role
        }
    
    def to_short_dict(self):
        """
        Returns the short dict version of the user object
        In order to be used in mongo db.
        """
        return {
            "username": self.username,
            "password": self.password,
        }
