import unittest
from car.car import Car
import datetime

class TestCar(unittest.TestCase):
    def test_CarConstructor(self):
        car = Car('Car', 'DETAILS', 2018, 20000, 10000,
            datetime.datetime.now(),
            datetime.datetime.now(),
            datetime.datetime.now(),
            datetime.datetime.now()
            )

        with self.assertRaises(TypeError):
            car = Car("Audi", 'No details', "2020", 2000, 10000,            
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now())
        
        with self.assertRaises(TypeError):
            car = Car("Audi", 'No details', 2020, "Hello", 10000,            
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now())
        
        with self.assertRaises(TypeError):
            car = Car("Audi", 'No details', 2020, 2000, "1MilKm",            
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now(),
                datetime.datetime.now())
        

if __name__ == '__main__':
    unittest.main()