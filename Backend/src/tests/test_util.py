import unittest
from util.util import *
import datetime

class TestUtil(unittest.TestCase):
    def test_Base64Encoding(self):
        string = 'hello'
        base64string = 'aGVsbG8='
        result = encode_to_base64(string)
        self.assertEqual(base64string, result)

        bad_string = 1234
        base64string = 'MTIzNA=='
        # wrong argument passed (int not string)
        with self.assertRaises(AttributeError):
            result = encode_to_base64(bad_string)
        
       
    def test_GetUserRoleFromExpiredToken(self):
        # JWT Token
        token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTExNzk4OTEsImlhdCI6MTYxMTE3OTg2MSwic3ViIjoibHVjaWFuX2FkbWluIiwicm9sZSI6ImFkbWluIn0.wame4afl5jEV2T7TpoHlExx_8x-y3vjEOdLudvcQzak'
        user, role = get_user_role_from_expired_token(token)
        expected_user = 'lucian_admin'
        expected_role = 'admin'
        self.assertEqual(user, expected_user)
        self.assertEqual(role, expected_role)

    def test_convertFromJsonToDatetime(self):
        json = {'year': 1998, 'month': 5, 'day': 25}
        result = convert_from_json_to_datetime(json)
        expected_result = datetime.datetime(1998,5,25)
        self.assertEqual(result, expected_result)

    def test_DateToString(self):
        date = datetime.datetime(1998,5,25)
        expected_result = '1998-05-25'
        result = date_to_string(date)
        self.assertEqual(result, expected_result)

        string_date = '2020-10-10'
        result = date_to_string(string_date)
        self.assertEqual(result, string_date)

if __name__ == '__main__':
    unittest.main()