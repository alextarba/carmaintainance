import unittest
from user.user import User
import datetime

class TestCar(unittest.TestCase):
    def test_UserConstructor(self):
        # (username: str, password: str, role=None, email=None, fullname=None)
        user = User("test_user", "pasword", "admin", "user@test.com", "Testing User")

        user = User("test_user", "pasword", "user")

        with self.assertRaises(ValueError):
            user = User("test_user", "pasword", "", "user@test.com", "Testing User")

        with self.assertRaises(ValueError):
            user = User("test_user", "pasword", "student")
        
        with self.assertRaises(ValueError):
            user = User("test_user", "pasword", "student", "")

        with self.assertRaises(ValueError):
            user = User("test_user", "pasword", "student", "", "")
        
if __name__ == '__main__':
    unittest.main()