class Person(object):
    """
    Parent class for User and Admin.
    """
    def __init__(self, password, email=None, fullname=None):
        """
        Initializes the instance.
        """
        # only password is mandatory
        self.password = password
        if email:
            self.email = email
        if fullname:
            self.fullname = fullname
        

    @property
    def email(self) -> str:
        """
        Retrieves the email.
        """
        return self._email

    @email.setter
    def email(self, email: str):
        """
        Sets the  email

        Args:
            email: The choosen email.
        """
        if email is None or email == "":
            raise ValueError("Must supply a valid email!")

        self._email = email

    @property
    def fullname(self) -> str:
        """
        Retrieves the fullname.
        """
        return self._fullname
    
    @fullname.setter
    def fullname(self, fullname: str):
        """
        Sets the  fullname

        Args:
            username: The choosen fullname at sign-up.
        """
        if fullname is None or fullname == "":
            raise ValueError("Must supply a valid fullname!")

        self._fullname = fullname

    @property
    def password(self) -> str:
        """
        Retrieves the password.
        """
        return self._password
    
    @password.setter
    def password(self, password: str):
        """
        Sets the  password

        Args:
            password: The choosen password at sign-up.
        """
        if password is None or password == "":
            raise ValueError("Must supply a valid password!")

        self._password = password