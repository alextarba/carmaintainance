import datetime
import json
import base64

def convert_from_json_to_datetime(json):
    return datetime.datetime(json['year'],json['month'],json['day'])

def date_to_string(date):
    # date is already in string format
    if isinstance(date, str):
        return date
    return date.strftime("%Y-%m-%d")

def get_user_role_from_expired_token(token):
    # get payload from token (base64 format)
    payload = token.split('.')[1]
    # add padding in order to be decoded
    payload = payload + '=' * (-len(payload) % 4)
    # convert from base64 to byte array
    bytes = base64.b64decode(payload)
    # convert from bytes to string
    string = bytes.decode("utf-8")
    # convert from string to JSON
    json_payload = json.loads(string)

    return json_payload['sub'], json_payload['role'] 

def encode_to_base64(string):
    """
    Returns base64 version of given string.
    """
    message_bytes = string.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')
    return base64_message

def get_user_car_list(username, cars_db):
    """
    Returns a string with a car list for each username passed from database.
    """
    cars = cars_db.read({'username': username})
    result = ""
    for car in cars:
        result += car['Name'] + " " + str(car['Year']) + ", "
    
    if result:
        # cars were found so removing extra comma and space
        return result[:len(result) - 2]
    
    # no car was found so 
    return "-"
