import datetime
from util.util import convert_from_json_to_datetime, date_to_string
# Car keys
NAME = 'Name'
DETAILS = 'Details'
YEAR = 'Year'
KILOMETERS = 'ActualKilometers'
LAST_REVISION_KM = 'LastRevisionKm'
LAST_REVISION_DATE = 'LastRevisionDate'
LAST_PTI = 'LastPti'
LAST_VIG = 'LastVig'
LAST_INSURANCE = 'LastInsurance'

class Car():
    def __init__(self, name, details, year,kilometers, last_revision_km, last_revision_date, last_pti, last_vignette, last_insurance):
        """
        Initializes the instance.
        """
        self.name = name
        self.details = details
        self.year = year
        self.kilometers = kilometers
        self.last_revision_km = last_revision_km
        self.last_revision_date = last_revision_date
        self.last_pti = last_pti
        self.last_vignette = last_vignette
        self.last_insurance = last_insurance
        

    @property
    def name(self) -> str:
        """
        Retrieves the name.
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """
        Sets the  name

        Args:
            name: The choosen car name.
        """
        if name is None or name == "":
            raise ValueError("Must supply a valid name!")

        self._name = name

    @property
    def details(self) -> str:
        """
        Retrieves the details.
        """
        return self._details
    
    @details.setter
    def details(self, details: str):
        """
        Sets the car details.

        Args:
            details: Input from details field from GUI.
        """
        if details is None or details == "":
            raise ValueError("Must supply a valid fullname!")

        self._details = details

    @property
    def year(self) -> int:
        """
        Retrieves the year.
        """
        return self._year
    
    @year.setter
    def year(self, year: int):
        """
        Sets the  year

        Args:
            year: The choosen year from GUI field.
        """
        if year is None or year < 1800 or year > 2021:
            raise ValueError("Must supply a valid year!")

        self._year = year

    @property
    def kilometers(self) -> int:
        """
        Retrieves the kilometers.
        """
        return self._kilometers
    
    @kilometers.setter
    def kilometers(self, kilometers: int):
        """
        Sets the kilometers.

        Args:
            kilometers: The choosen kilometers from GUI field.
        """
        if kilometers is None or kilometers < 0:
            raise ValueError("Must supply a valid number of kilometers!")

        self._kilometers = kilometers

    @property
    def last_revision_km(self) -> int:
        """
        Retrieves the last revision kilometers.
        """
        return self._last_revision_km
    
    @last_revision_km.setter
    def last_revision_km(self, last_revision_km: int):
        """
        Sets the last_revision_km.

        Args:
            last_revision_km: The choosen last_revision_km from GUI field.
        """
        if last_revision_km is None or last_revision_km < 0:
            raise ValueError("Must supply a valid number of kilometers!")

        self._last_revision_km = last_revision_km

    @property
    def last_revision_date(self) -> datetime:
        """
        Retrieves the last revision as date.
        """
        return self._last_revision_date
    
    @last_revision_date.setter
    def last_revision_date(self, last_revision_date: datetime):
        """
        Sets the last_revision_date.

        Args:
            last_revision_date: The choosen last_revision_date from GUI field.
        """
        if last_revision_date is None:
            raise ValueError("Must supply a valid date!")

        self._last_revision_date = last_revision_date
   
    @property
    def last_pti(self) -> datetime:
        """
        Retrieves the last PTI as date.
        """
        return self._last_pti
    
    @last_pti.setter
    def last_pti(self, last_pti: datetime):
        """
        Sets the last_pti.

        Args:
            last_pti: The choosen last_pti from GUI field.
        """
        if last_pti is None:
            raise ValueError("Must supply a valid date!")

        self._last_pti = last_pti

    @property
    def last_vignette(self) -> datetime:
        """
        Retrieves the last vignette as date.
        """
        return self._last_vignette
    
    @last_vignette.setter
    def last_vignette(self, last_vignette: datetime):
        """
        Sets the last_vignette.

        Args:
            last_vignette: The choosen last_vignette from GUI field.
        """
        if last_vignette is None:
            raise ValueError("Must supply a valid date")

        self._last_vignette = last_vignette

    @property
    def last_insurance(self) -> datetime:
        """
        Retrieves the last insurance as date.
        """
        return self._last_insurance
    
    @last_insurance.setter
    def last_insurance(self, last_insurance: datetime):
        """
        Sets the last_insurance.

        Args:
            last_insurance: The choosen last_insurance from GUI field.
        """
        if last_insurance is None:
            raise ValueError("Must supply a valid date")

        self._last_insurance = last_insurance

    def to_dict(self, username):
        """
        Returns the dict version of the car object
        In order to be used in mongo db.
        """
        return {
            "username": username,
            NAME: self.name,
            DETAILS: self.details,
            YEAR: self.year,
            KILOMETERS: self.kilometers,
            LAST_REVISION_KM: self.last_revision_km,
            LAST_REVISION_DATE: self.last_revision_date,
            LAST_PTI: self.last_pti,
            LAST_VIG: self.last_vignette,
            LAST_INSURANCE: self.last_insurance
        }
    
    def to_json(self):
        """
        Returns the JSON version of the car object
        In order to be sent back to Frontend
        """
        return {
            NAME: self.name,
            DETAILS: self.details,
            YEAR: self.year,
            KILOMETERS: self.kilometers,
            LAST_REVISION_KM: self.last_revision_km,
            LAST_REVISION_DATE: date_to_string(self.last_revision_date),
            LAST_PTI: date_to_string(self.last_pti),
            LAST_VIG: date_to_string(self.last_vignette),
            LAST_INSURANCE: date_to_string(self.last_insurance)
        }