#!/bin/bash

# Restart mongo db
sudo systemctl restart mongodb
sudo systemctl status mongodb

# Run the flask server
export FLASK_APP=src/server.py
python3 -m venv env
flask run
