# CarMaintainance

## Backend side
---
### **How to start the (backend) server**
**Ubuntu**
---
1. Make sure that MongoDB Service is active by running:
```
sudo systemctl restart mongodb
sudo systemctl status mongodb
```

2. Open a new terminal and then run:
```
export FLASK_APP=src/server.py
flask run
```
3. The backend server should be up and running on: **https://127.0.0.1:5000/**

### **How to setup the server**
### More information about it here: https://code.visualstudio.com/docs/python/tutorial-flask
1. Make sure you have flask installed.
> On **Windows**: 
```
python -m venv env
pip install Flask
pip install -U flask-cors
pip install pyjwt
```

> On **Ubuntu**: 
```
sudo apt-get install python3-venv 
python3 -m venv env
pip install flask
pip install -U flask-cors
pip install pyjwt
```

2. To run the application you can either use the flask command or python’s -m switch with Flask. Before you can do that you need to tell your terminal the application to work with by exporting the FLASK_APP environment variable.
> On **Windows** open CMD(Command Prompt): ```set FLASK_APP=src/server.py```

> On **Ubuntu**: ```export FLASK_APP=src/server.py```

3. Start the server.
> Run either: ```flask run``` or ```python -m flask run```

4. Check if your server is up and running on the address displayed by flask by opening it in your favorite browser.
> Example: ```http://127.0.0.1:5000/```
---

### **How to install mongoDB**
> On **Ubuntu**  
> 1. First, update the packages list to have the most recent version of the repository listings: ```sudo apt update```
> 2. Now install the MongoDB package itself: ```sudo apt install -y mongodb```
> 3. Check the service's status: ```sudo systemctl status mongodb```
> 4. Make sure that the python package `pymongo` is installed using `pip3 install pymongo`